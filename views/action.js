
var gDiscount = 0;
var gSelectedMenuStructure = {
  menuName: "...",    // S, M, L
  duongKinhCM: 0,
  suongNuong: 0,
  saladGr: 0,
  drink: 0,
  priceVND: 0
}

var gSelectedPizzaType = "...";
//---------------------------------------------------------------------------------------------------------------------------------------------------------
function onBtnChonS() {
  gSelectedMenuStructure = getInfoCombo("Small", 20, 2, 200, 2, 150000);
  doiMauNutChonCombo("S");
}
function onBtnChonM() {
  gSelectedMenuStructure = getInfoCombo("Medium", 25, 4, 300, 3, 200000,);
  doiMauNutChonCombo("M");
}
function onBtnChonL() {
  gSelectedMenuStructure = getInfoCombo("Large", 30, 8, 500, 4, 250000,);
  doiMauNutChonCombo("L");
}
function onBtnChonGaClick() {
  gSelectedPizzaType = getInfoTypeOfPizza("Hải sản");
  doiMauNutChonLoai("HaiSan");
}
function onBtnHaiSanClick() {
  gSelectedPizzaType = getInfoTypeOfPizza("Dăm bông");
  doiMauNutChonLoai("dambong");
}
function onBtnThitNuongClick() {
  gSelectedPizzaType = getInfoTypeOfPizza("Gà");
  doiMauNutChonLoai("ga");
}
function getInfoCombo(paramMenu, paramDai, paramSuon, paramSalad, paramDoUong, paramTien) {
  var vInfoCombo = {
    menu: paramMenu,
    doDai: paramDai,
    suon: paramSuon,
    salad: paramSalad,
    doUong: paramDoUong,
    soTien: paramTien
  }
  console.log("%cMenu được chọn là: " + vInfoCombo.menu, "color: blue");
  console.log("Dài: " + vInfoCombo.doDai +"cm");
  console.log("Sườn: " + vInfoCombo.suon);
  console.log("Salad:  " + vInfoCombo.salad + "g") ;
  console.log("Nước ngọt:  " + vInfoCombo.doUong);
  console.log("soTien: " + vInfoCombo.soTien + " VNĐ");
  return vInfoCombo;
}
function getInfoTypeOfPizza(paramType) {
  var typeOfPizza = {
    type: paramType
  }
  console.log("%cLoại pizza được chọn là: " + typeOfPizza.type, "color: yellow");
  return typeOfPizza.type;
}
function doiMauNutChonCombo(paramMau) {
  var vBtnChonS = document.getElementById("btn-chon-S");
  var vBtnChonM = document.getElementById("btn-chon-M");
  var vBtnChonL = document.getElementById("btn-chon-L");
  if (paramMau == "S") {
    vBtnChonS.className = "btn btn-selected btn-block";
    vBtnChonM.className = "btn btn-select btn-block";
    vBtnChonL.className = "btn btn-select btn-block";
  } else if (paramMau == "M") {
    vBtnChonS.className = "btn btn-select btn-block";
    vBtnChonM.className = "btn btn-selected btn-block";
    vBtnChonL.className = "btn btn-select btn-block";
  } else if (paramMau == "L") {
    vBtnChonS.className = "btn btn-select btn-block";
    vBtnChonM.className = "btn btn-select btn-block";
    vBtnChonL.className = "btn btn-selected btn-block";
  }
}
function doiMauNutChonLoai(paramMauLoai) {
  var vBtnChonGa = document.getElementById("btn-chon-ga");
  var vBtnChonHaiSan = document.getElementById("btn-hai-san");
  var vBtnChonThitNuong = document.getElementById("btn-thit-nuong");
  if (paramMauLoai == "HaiSan") {
    vBtnChonGa.className = "btn btn-selected btn-block";
    vBtnChonHaiSan.className = "btn btn-select btn-block";
    vBtnChonThitNuong.className = "btn btn-select btn-block";
  } else if (paramMauLoai == "dambong") {
    vBtnChonGa.className = "btn btn-select btn-block";
    vBtnChonHaiSan.className = "btn btn-selected btn-block";
    vBtnChonThitNuong.className = "btn btn-select btn-block";
  } else if (paramMauLoai == "ga") {
    vBtnChonGa.className = "btn btn-select btn-block";
    vBtnChonHaiSan.className = "btn btn-select btn-block";
    vBtnChonThitNuong.className = "btn btn-selected btn-block";
  }
}
function loadDataIntoSltWater() {
    $.ajax({
      url:  "http://42.115.221.44:8080/devcamp-pizza365/drinks",
      type: "GET",
      success: function(res) {
        console.log(res);
        for (i = 0; i < res.length; i++) {
        $("#slt-do-uong").append( 
            $('<option>', {
                value: res[i].maNuocUong,
               text: res[i].tenNuocUong
            }
          ))
      } }
    })
}
function onBtnKiemTradonClick() {
  var vOrder = {
    menuSelected: null,
    fullName: "",
    email: "",
    phoneNumber: "",
    address: "",
    message: "",
    voucherId: "",
    priceVND: 0,
    doUong: "",
    pizzaType: null
  }
  getInfoOFOrder(vOrder);
  var vKiemTra = kiemTraInfoDonHang(vOrder);
  getVoucherID(vOrder.voucherId);
  if (vKiemTra == true) {
    // showOrderData(vOrder);
    $("#modalConfirmOrder").modal("show");
    loadDataIntoModal(vOrder)
  }

}
function getInfoOFOrder(paramOrder) {
  var vInpHoTen = document.getElementById("inp-fullname");
  var vInpEmail = document.getElementById("inp-email");
  var vInpSdt = document.getElementById("inp-dien-thoai");
  var vInpAddress = document.getElementById("inp-dia-chi");
  var vInpMessage = document.getElementById("inp-message");
  var vInpMaGiamGia = document.getElementById("inp-giam-gia");
  var vDoUong = document.getElementById("slt-do-uong");

  paramOrder.menuSelected = gSelectedMenuStructure;
  paramOrder.fullName = vInpHoTen.value.trim();
  paramOrder.email = vInpEmail.value.trim();
  paramOrder.phoneNumber = vInpSdt.value.trim();
  paramOrder.address = vInpAddress.value.trim();
  paramOrder.message = vInpMessage.value.trim();
  paramOrder.voucherId = vInpMaGiamGia.value.trim();
  paramOrder.priceVND = gSelectedMenuStructure.soTien;
  paramOrder.pizzaType = gSelectedPizzaType;
  paramOrder.doUong = vDoUong.value;

}
function kiemTraInfoDonHang(paramKiemTra) {
  if (paramKiemTra.menuSelected.menu == null) {
    alert("Mời bạn chọn Combo Pizza");
    return false;
  } if (gSelectedPizzaType == "...") {
    alert("Mời bạn chọn loại Pizza");
    return false;
  } if (paramKiemTra.doUong == "0") {
    alert("Mời bạn chọn loại đồ uống");
    return false;
  } if (paramKiemTra.fullName == "") {
    alert("Mời bạn nhập tên");
    return false;
  } if (kiemTraCoFaiEmailHayKo(paramKiemTra.email) == false) {
    alert("Mời bạn nhập đúng email");
    return false;
  } if (paramKiemTra.phoneNumber == "" || isNaN(paramKiemTra.phoneNumber) == true) {
    alert("Mời bạn nhập đúng SDT");
    return false;
  } if (paramKiemTra.address == "") {
    alert("Mời bạn nhập địa chỉ");
    return false;
  }if (paramKiemTra.voucherId == "") {
    alert("Mời bạn nhập mã giảm giá");
    return false;
  }
   if (paramKiemTra.message == "") {
    alert("Mời bạn nhập lời nhắn");
    return false;
  } 
  return true;
}
function kiemTraCoFaiEmailHayKo(paramEmail) {
  if (paramEmail == "" || !paramEmail.includes("@") || paramEmail.startsWith("@") || paramEmail.endsWith("@")) {
    return false;
  } return true;
}
function getVoucherID(paramVoucherId) {
  "use strict";
  $.ajax({
    url:  "http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/" + paramVoucherId,
    type: "GET",
    async: false,
    data: null,
    success: function(res) {
        console.log("Tìm thấy voucher ID này: " + res.phanTramGiamGia);
        gDiscount = res.phanTramGiamGia;
    },
    error : function(resErr) {
        console.log("Không tìm thấy voucher " + resErr);
    }
  })
}
function loadDataIntoModal(paramData) {
    $("#modal-inp-fullname").val(paramData.fullName)
    $("#modal-inp-dien-thoai").val(paramData.phoneNumber)
    $("#modal-inp-dia-chi").val(paramData.address)
    $("#modal-inp-message").val(paramData.message)
    $("#modal-inp-discount").val(paramData.voucherId)
    $("#modalTextPizzaInfo").val("Xác nhận: " + $("#modal-inp-fullname").val() +"," + $("#modal-inp-dien-thoai").val()+ "," +$("#modal-inp-dia-chi").val() +"\n" +
    "Kích cỡ (combo): " + paramData.menuSelected.menu  +", sườn nướng: " + paramData.menuSelected.suon +", nước ngọt: " + paramData.menuSelected.doUong +"..." +"\n" +
    "Loại pizza được chọn: " + gSelectedPizzaType + ", giá: " + paramData.priceVND + "VND" + ", mã giảm giá: " +  $("#modal-inp-discount").val() + "\n" +
    "Phải thanh toán: " +  paramData.priceVND * (1 - gDiscount / 100) + " VNĐ" + " (giảm giá: " + gDiscount +"%" + ")"

)}
$("#btn-confirm-order").on('click',function() {
    onBtnGuiDonClick()
})
function onBtnGuiDonClick() {
  var vNewOrder = {
    kichCo: "",
    duongKinh: "",
    suon: "",
    salad: "",
    loaiPizza: "",
    idVourcher: "",
    idLoaiNuocUong: "",
    soLuongNuoc: 0,
    hoTen: "",
    thanhTien: "",
    email: "",
    soDienThoai: "",
    diaChi: "",
    loiNhan: ""
  }
  var vinfoData = getDataIntoNewForm(vNewOrder);
  console.log(vinfoData);
  $.ajax({
    url: "http://42.115.221.44:8080/devcamp-pizza365/orders",
    type: "POST",
    contentType : "application/json;charset=UTF-8",
    data: JSON.stringify(vinfoData),
    success: function(res) {
        console.log(res)
        $("#modalConfirmOrder").modal("hide");
        $("#modal-success-order").modal('show');
        $("#inp-ma-success").val(res.orderId)
    },
    error: function(resErr) {
        console.log(resErr)
    }
  })
}
function getDataIntoNewForm(paramData) {
  var vDoUong = document.getElementById("slt-do-uong");
  paramData.kichCo = gSelectedMenuStructure.menu;
  paramData.duongKinh = gSelectedMenuStructure.doDai;
  paramData.suon = gSelectedMenuStructure.suon;
  paramData.salad = gSelectedMenuStructure.salad;
  paramData.loaiPizza = gSelectedPizzaType;
  paramData.soLuongNuoc = gSelectedMenuStructure.doUong;
  paramData.thanhTien = gSelectedMenuStructure.soTien
  paramData.idLoaiNuocUong = vDoUong.options[vDoUong.selectedIndex].value;
  paramData.idVourcher = $("#modal-inp-discount").val()
  paramData.hoTen = $("#modal-inp-fullname").val()
  paramData.email = $("#inp-email").val()
  paramData.soDienThoai =  $("#modal-inp-dien-thoai").val()
  paramData.diaChi =  $("#modal-inp-dia-chi").val()
  paramData.loiNhan =  $("#modal-inp-message").val()
  return paramData;
}


